package simulation;

import com.espertech.esper.compiler.client.EPCompileException;
import com.espertech.esper.runtime.client.EPDeployException;
import com.espertech.esper.runtime.client.EPEventService;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.log4j.Logger;
import utils.HumanActivity;
import utils.EsperUtils;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

/**
 * This class is used to simulate, in real time, the data. As you can see, in line 53, there is a Thread.sleep(100)
 * which sleeps the app 100 milliseconds in order to simulate the real time behaviour of the sensor nodes.
 * <p>
 * This class is useful for testing purposes, but Simulation.java can perform the same and instantly using ext_time data
 * windows.
 *
 * @author David Corral-Plaza <david.corral@uca.es>
 */
public class RealTimeSimulation {

    static public Logger logger = Logger.getLogger(Simulation.class);

    public static void main(String[] args) throws EPDeployException, EPCompileException, InterruptedException {

        new EsperUtils();
        EsperUtils.createEPL("@Eventrepresentation(json) @Name('StandingOrSitting') " +
                "INSERT INTO StandingOrSitting " +
                "SELECT 'Standing or Sitting' as position, a1.p11, count(a1), count(a2),count(a3),count(a4)" +
                " FROM pattern [every ( ((a1= HumanActivity(a1.p2 = 1 and a1.p4 between 0.75 and 1.25)) " +
                "and (a2 = HumanActivity(a2.p2=2 and a2.p4 between 0.75 and 1.25)) " +
                "and (a3 = HumanActivity(a3.p2=3 and a3.p4 between 0.75 and 1.25)) " +
                "and (a4 = HumanActivity(a4.p2=4 and a4.p4 between 0.75 and 1.25))))]#time(200 msec);");
        EPEventService epEventService = EsperUtils.getEpRuntime().getEventService();
        String path = "D:\\Github\\TimeSeriesClassification\\resources\\april-luigi\\4nodes\\backfall\\";
        List<HumanActivity> s1 = readCsvFile(path + "S1data.csv", 1);
        List<HumanActivity> s2 = readCsvFile(path + "S2data.csv", 2);
        List<HumanActivity> s3 = readCsvFile(path + "S3data.csv", 3);
        List<HumanActivity> s4 = readCsvFile(path + "S4data.csv", 4);
        for (int i = 0; i < s1.size(); i++) {
            epEventService.sendEventBean(s1.get(i), "HumanActivity");
            epEventService.sendEventBean(s2.get(i), "HumanActivity");
            epEventService.sendEventBean(s3.get(i), "HumanActivity");
            epEventService.sendEventBean(s4.get(i), "HumanActivity");
            Thread.sleep(100);
        }

    }

    private static List<HumanActivity> readCsvFile(String filePath, int sensorId) {
        List<HumanActivity> sensorReads = new ArrayList<HumanActivity>();

        try {
            BufferedReader fileReader = new BufferedReader(new FileReader(filePath));
            CSVParser csvParser = new CSVParser(fileReader,
                    CSVFormat.DEFAULT.withFirstRecordAsHeader().withIgnoreHeaderCase().withTrim());

            Iterable<CSVRecord> csvRecords = csvParser.getRecords();
            for (CSVRecord csvRecord : csvRecords) {
                HumanActivity HumanActivity = new HumanActivity(
                        sensorId,
                        Double.parseDouble(csvRecord.get("A-Mag[G]")),
                        Double.parseDouble(csvRecord.get("A-x[G]")),
                        Double.parseDouble(csvRecord.get("A-y[G]")),
                        Double.parseDouble(csvRecord.get("A-z[G]")),
                        Double.parseDouble(csvRecord.get("G-Mag[rad/s]")),
                        Double.parseDouble(csvRecord.get("G-x[rad/s]")),
                        Double.parseDouble(csvRecord.get("G-y[rad/s]")),
                        Double.parseDouble(csvRecord.get("G-z[rad/s]")),
                        (Long.parseLong(csvRecord.get("Time[s]")))
                );

                sensorReads.add(HumanActivity);
            }

            fileReader.close();
            csvParser.close();

        } catch (Exception e) {
            logger.error("Reading CSV Error!");
            e.printStackTrace();
        }

        return sensorReads;
    }
}
