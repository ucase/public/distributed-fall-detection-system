package utils;

import com.espertech.esper.common.client.EPCompiled;
import com.espertech.esper.common.client.configuration.Configuration;
import com.espertech.esper.common.client.dataflow.core.EPDataFlowInstance;
import com.espertech.esper.common.internal.event.avro.AvroSchemaEventType;
import com.espertech.esper.compiler.client.CompilerArguments;
import com.espertech.esper.compiler.client.EPCompileException;
import com.espertech.esper.compiler.client.EPCompiler;
import com.espertech.esper.compiler.client.EPCompilerProvider;
import com.espertech.esper.runtime.client.*;
import org.apache.avro.Schema;
import org.apache.avro.generic.GenericData;
import org.apache.log4j.Logger;
import simulation.Simulation;

import java.util.*;


/**
 * @author David Corral-Plaza <david.corral@uca.es>
 */

public class EsperUtils {

    static public Logger logger = Logger.getLogger(Simulation.class);
    private static EPCompiler epCompiler;
    private static EPRuntime epRuntime;
    private static Configuration configuration;

    /**
     * Constructor
     */
    public EsperUtils() {
        synchronized (EsperUtils.class) {
            logger.info("** Starting Esper Engine **");

            if (configuration == null) {
                logger.info("*** Creating configuration ***");
                configuration = new Configuration();
                configuration.getCommon().addEventType(HumanActivity.class);
            }

            if (epCompiler == null) {
                logger.info("*** Starting Esper Compiler ***");
                epCompiler = EPCompilerProvider.getCompiler();
            }

            if (epRuntime == null) {
                logger.info("*** Starting Esper Runtime ***");
                epRuntime = EPRuntimeProvider.getDefaultRuntime(configuration);
            }

        }
    }

    /**
     * Returns the CompilerArguments each time
     * that a EPL code has to be compiled
     *
     * @return arguments with the global configuration
     */
    public static CompilerArguments getCompilerArguments() {
        CompilerArguments arguments = new CompilerArguments(getEpRuntime().getRuntimePath());
        arguments.setConfiguration(configuration);
        return arguments;
    }

    /**
     * Returns the epCompiler that have been set before
     *
     * @return the epCompiler of the engine
     */
    public static EPCompiler getEpCompiler() {
        synchronized (EsperUtils.class) {
            if (epCompiler == null) {
                logger.info("epCompiler is not defined");
                throw new RuntimeException("Unable to continue because epCompiler is not defined!");
            }
        }
        return epCompiler;
    }

    /**
     * Returns the epRuntime that have been set before
     *
     * @return the epRuntime of the engine
     */
    public static EPRuntime getEpRuntime() {
        synchronized (EsperUtils.class) {
            if (epRuntime == null) {
                logger.info("epRuntime is not defined");
                throw new RuntimeException("Unable to continue because epRuntime is not defined!");
            }
        }
        return epRuntime;
    }

    /**
     * Returns the deployment id that have been set before
     *
     * @return the epDeploymentId used at runtime
     */
    public static String getDeploymentId() {
        logger.info("Deployments Ids: " + Arrays.asList(getEpRuntime().getDeploymentService().getDeployments()));
        return getEpRuntime().getDeploymentService().getDeployments()[0];
    }

    /**
     * Add a generic listener to each EPL statement
     *
     * @param statement the EPStatement which will have the listener added
     */
    static int cont = 0;

    private static void addGenericListener(EPStatement statement) {
        statement.addListener((newComplexEvents, oldComplexEvents, detectedEventPattern, epRuntime) -> {
            if (newComplexEvents != null) {
                cont++;
                logger.info(cont + "|||" + newComplexEvents[0].getUnderlying());

                /*HumanActivity complexEvent = (HumanActivity) (newComplexEvents[0].getUnderlying());
                logger.info("P"+complexEvent.getP2()+ " | P11: " + complexEvent.getP11());*/
            }
        });
    }

    /**
     * Receives and EPL and add it to the Esper Engine with a generic listener
     *
     * @param epl the EPL to be added to the engine
     * @throws EPCompileException fail compiling the EPL
     * @throws EPDeployException  fail deploying the EPL
     */
    public static String createEPL(String epl) throws EPCompileException, EPDeployException {
        //addGenericListener(getEpRuntime().getDeploymentService().getStatement(deployNewEventPattern(epl).getDeploymentId(), name));
        EPStatement ep = deployNewEventPattern(epl).getStatements()[0];
        addGenericListener(ep);
        return ep.getDeploymentId();
    }

    /**
     * Receives a string with all ids and iterates for undeploy them
     *
     * @param ids The ids as String to be undeployed
     */
    public static void undeployIds(String ids) {
        String[] deploymentIds = ids.replaceAll(" ", "").split(",");
        for (String id : deploymentIds) {
            undeploy(id);
        }
    }

    /**
     * Receives an ArrayList with all ids and iterates for undeploy them
     *
     * @param deploymentIds The ids as ArrayList to be undeployed
     */
    public static boolean undeployIds(ArrayList<String> deploymentIds) {
        for (String id : deploymentIds) {
            undeploy(id);
        }
        return true;
    }

    /**
     * Auxiliary function to undeploy ids
     */
    private static boolean undeploy(String id) {
        logger.info("** Undeploying " + id);
        try {
            getEpRuntime().getDeploymentService().undeploy(id);
            logger.info("** " + id + " undeployed successfully");
            return true;
        } catch (EPUndeployException e) {
            String deploymentId = e.getMessage().substring(e.getMessage().length() - 37, e.getMessage().length() - 1);
            if (deploymentId.length() == 36) {
                logger.info("*** There is a precondition that has to be undeployed first: " + deploymentId);
                ArrayList<String> deploymentIds = new ArrayList<>();
                deploymentIds.add(deploymentId);
                deploymentIds.add(id);
                return undeployIds(deploymentIds);
            } else
                return true;
        }
    }

    /**
     * Creates a new dataflow
     *
     * @param epl  The dataflow to be deployed
     * @param name The dataflow's name
     * @throws EPCompileException fail compiling the dataflow
     * @throws EPDeployException  fail deploying the dataflow
     */
    public static void createDataflow(String epl, String name) throws EPCompileException, EPDeployException {
        logger.info("Deploying dataflow '" + name + "'");
        EPDataFlowInstance instance =
                getEpRuntime().getDataFlowService().instantiate(deployNewEventPattern(epl).getDeploymentId(), name);

        logger.info("Dataflow '" + name + "' instantiated");
        instance.run();
        logger.info("Dataflow '" + name + "' finished");
    }

    /**
     * Calls to the compiler and then deploy the compiled EPL sentence
     *
     * @param epl The EPL sentence to be compiled
     * @return The EPL sentence deployed
     * @throws EPCompileException fail compiling the EPL
     * @throws EPDeployException  fail deploying the EPL
     */
    public static EPDeployment deployNewEventPattern(String epl) throws EPCompileException, EPDeployException {
        return getEpRuntime().getDeploymentService().deploy(compileNewEventPattern(epl));
    }

    /**
     * Compiles an EPL sentence using the runtime configuration
     *
     * @param epl The EPL sentence to be compiled
     * @return The EPL sentence compiled
     * @throws EPCompileException fail compiling the EPL
     */
    public static EPCompiled compileNewEventPattern(String epl) throws EPCompileException {
        return getEpCompiler().compile(epl, getCompilerArguments());
    }

    /**
     * Creates a new event type at runtime
     *
     * @param epl The sentence with the new schema to create
     * @throws EPCompileException fail compiling the Schema
     * @throws EPDeployException  fail deploying the Schema
     */
    public static String addNewSchema(String epl) throws EPCompileException, EPDeployException {
        logger.info("Adding new schema: " + epl);
        return deployNewEventPattern(epl).getDeploymentId();
    }

    /**
     * Checks if the event type name is already in use
     *
     * @param eventTypeName to check if exists
     * @return if the event type name exists or not
     */
    public static boolean eventTypeExists(String eventTypeName) {
        for (String deploymentId : getEpRuntime().getDeploymentService().getDeployments())
            if (getEpRuntime().getEventTypeService().getEventType(deploymentId, eventTypeName) != null)
                return true;
        return false;
    }

    /**
     * Adds a new Avro Event to the Esper engine
     *
     * @param avroEvent     the Avro Event already created
     * @param eventTypeName the event type name of the Avro Event
     */
    public static void sendAvroEvent(GenericData.Record avroEvent, String eventTypeName) {
        getEpRuntime().getEventService().sendEventAvro(avroEvent, eventTypeName);
    }

    /**
     * Extracts the Schema from an Avro Event already registered in the engine
     *
     * @param eventTypeName the event type name of the event type to extract the schema
     * @return the Schema of the event type
     */
    static Schema getAvroSchema(String eventTypeName) {
        return (Schema) ((AvroSchemaEventType)
                getEpRuntime().getEventTypeService().getEventType(getDeploymentId(), eventTypeName)).getSchema();
    }

    /**
     * Set up default options
     * It is useful for testing purposes
     */
    public static void setDefaults() throws EPCompileException, EPDeployException {
        addNewSchema("@public @buseventtype @EventRepresentation(avro) create avro schema WaterMeasurement as (serialNumber string, dateTime string, volumeM3 double, volumeL double, type string, batteryLevel int, batteryLevelStr string, sleepingTime integer, leakingTime integer, normalTime integer, starts integer)");
        deployNewEventPattern("@public create context IntervalSpanning300Seconds start @now end after 300 sec");
        createEPL("context IntervalSpanning300Seconds select count(*) from WaterMeasurement output snapshot when terminated");
    }

}
